-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 18 Septembre 2014 à 16:09
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `15km`
--
CREATE DATABASE IF NOT EXISTS `15km` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `15km`;

-- --------------------------------------------------------

--
-- Structure de la table `arrivee_15`
--

CREATE TABLE IF NOT EXISTS `arrivee_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temps` varchar(20) NOT NULL,
  `dossard` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `arrivee_75`
--

CREATE TABLE IF NOT EXISTS `arrivee_75` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temps` varchar(20) NOT NULL,
  `dossard` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `arrivee_benjamins`
--

CREATE TABLE IF NOT EXISTS `arrivee_benjamins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temps` varchar(20) NOT NULL,
  `dossard` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `arrivee_minimes`
--

CREATE TABLE IF NOT EXISTS `arrivee_minimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temps` varchar(20) NOT NULL,
  `dossard` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `arrivee_poussins`
--

CREATE TABLE IF NOT EXISTS `arrivee_poussins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temps` varchar(20) NOT NULL,
  `dossard` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `attribution_dossard`
--

CREATE TABLE IF NOT EXISTS `attribution_dossard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(10) NOT NULL,
  `debut_dossard` varchar(10) NOT NULL,
  `fin_dossard` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `attribution_dossard`
--

INSERT INTO `attribution_dossard` (`id`, `course`, `debut_dossard`, `fin_dossard`) VALUES
(1, '7.5', '1', '500'),
(2, '15', '501', '1000'),
(3, 'poussin', '1001', '1099'),
(4, 'benjamin', '1100', '1199'),
(5, 'minimes', '1200', '1299');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) NOT NULL,
  `age_debut` varchar(5) NOT NULL,
  `age_fin` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`, `age_debut`, `age_fin`) VALUES
(1, 'SENIOR', '22', '39'),
(2, 'VETERAN 1', '40', '49'),
(3, 'VETERAN 2', '50', '59'),
(4, 'VETERAN 3', '60', '69'),
(5, 'VETERAN 3', '70', '79'),
(6, 'VETERAN 4', '80', '89'),
(7, 'ESPOIR', '', ''),
(8, 'JUNIOR', '', ''),
(9, 'CADET', '', ''),
(10, 'MINIME', '', ''),
(11, 'BENJAMIN', '', ''),
(12, 'POUSSIN', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `dossards`
--

CREATE TABLE IF NOT EXISTS `dossards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_coureur` bigint(20) NOT NULL,
  `dossard` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dossard` (`dossard`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=213 ;

--
-- Contenu de la table `dossards`
--

INSERT INTO `dossards` (`id`, `id_coureur`, `dossard`) VALUES
(1, 3, 501),
(2, 4, 502),
(3, 5, 503),
(4, 6, 504),
(5, 7, 505),
(6, 8, 506),
(7, 10, 507),
(8, 11, 508),
(9, 12, 509),
(10, 14, 510),
(11, 15, 511),
(12, 16, 512),
(13, 17, 513),
(14, 18, 514),
(15, 20, 515),
(16, 21, 516),
(17, 22, 517),
(18, 23, 518),
(19, 24, 519),
(20, 25, 520),
(21, 26, 521),
(22, 27, 522),
(23, 28, 523),
(24, 29, 524),
(25, 30, 525),
(26, 31, 526),
(27, 32, 527),
(28, 33, 528),
(29, 34, 529),
(30, 36, 530),
(31, 37, 531),
(32, 40, 532),
(33, 42, 533),
(34, 43, 534),
(35, 46, 535),
(36, 47, 536),
(37, 52, 537),
(38, 53, 538),
(39, 54, 539),
(40, 55, 540),
(41, 56, 541),
(42, 57, 542),
(43, 58, 543),
(44, 59, 544),
(45, 60, 545),
(46, 61, 546),
(47, 62, 547),
(48, 65, 548),
(49, 66, 549),
(50, 67, 550),
(51, 68, 551),
(52, 71, 552),
(53, 75, 553),
(54, 76, 554),
(55, 77, 555),
(56, 79, 556),
(57, 80, 557),
(58, 81, 558),
(59, 82, 559),
(60, 84, 560),
(61, 85, 561),
(62, 86, 562),
(63, 87, 563),
(64, 88, 564),
(65, 91, 565),
(66, 92, 566),
(67, 93, 567),
(68, 94, 568),
(69, 95, 569),
(70, 96, 570),
(71, 99, 571),
(72, 100, 572),
(73, 105, 573),
(74, 107, 574),
(75, 108, 575),
(76, 112, 576),
(77, 113, 577),
(78, 118, 578),
(79, 119, 579),
(80, 120, 580),
(81, 121, 581),
(82, 122, 582),
(83, 123, 583),
(84, 124, 584),
(85, 125, 585),
(86, 126, 586),
(87, 127, 587),
(88, 128, 588),
(89, 130, 589),
(90, 131, 590),
(91, 132, 591),
(92, 133, 592),
(93, 134, 593),
(94, 136, 594),
(95, 137, 595),
(96, 141, 596),
(97, 147, 597),
(98, 148, 598),
(99, 149, 599),
(100, 151, 600),
(101, 152, 601),
(102, 153, 602),
(103, 154, 603),
(104, 155, 604),
(105, 157, 605),
(106, 158, 606),
(107, 160, 607),
(108, 161, 608),
(109, 163, 609),
(110, 168, 610),
(111, 169, 611),
(112, 173, 612),
(113, 174, 613),
(114, 176, 614),
(115, 180, 615),
(116, 183, 616),
(117, 186, 617),
(118, 187, 618),
(119, 188, 619),
(120, 189, 620),
(121, 190, 621),
(122, 192, 622),
(123, 194, 623),
(124, 197, 624),
(125, 198, 625),
(126, 199, 626),
(127, 203, 627),
(128, 1, 1),
(129, 2, 2),
(130, 9, 3),
(131, 13, 4),
(132, 19, 5),
(133, 35, 6),
(134, 38, 7),
(135, 39, 8),
(136, 41, 9),
(137, 44, 10),
(138, 45, 11),
(139, 48, 12),
(140, 49, 13),
(141, 50, 14),
(142, 51, 15),
(143, 63, 16),
(144, 64, 17),
(145, 69, 18),
(146, 70, 19),
(147, 72, 20),
(148, 73, 21),
(149, 74, 22),
(150, 78, 23),
(151, 83, 24),
(152, 89, 25),
(153, 90, 26),
(154, 97, 27),
(155, 98, 28),
(156, 101, 29),
(157, 102, 30),
(158, 103, 31),
(159, 104, 32),
(160, 106, 33),
(161, 109, 34),
(162, 110, 35),
(163, 111, 36),
(164, 114, 37),
(165, 115, 38),
(166, 116, 39),
(167, 117, 40),
(168, 129, 41),
(169, 135, 42),
(170, 138, 43),
(171, 139, 44),
(172, 140, 45),
(173, 142, 46),
(174, 143, 47),
(175, 144, 48),
(176, 145, 49),
(177, 146, 50),
(178, 150, 51),
(179, 156, 52),
(180, 159, 53),
(181, 162, 54),
(182, 164, 55),
(183, 165, 56),
(184, 166, 57),
(185, 167, 58),
(186, 170, 59),
(187, 171, 60),
(188, 172, 61),
(189, 175, 62),
(190, 177, 63),
(191, 178, 64),
(192, 179, 65),
(193, 181, 66),
(194, 182, 67),
(195, 185, 68),
(196, 191, 69),
(197, 193, 70),
(198, 195, 71),
(199, 196, 72),
(200, 200, 73),
(201, 201, 74),
(202, 202, 75),
(203, 204, 76),
(204, 205, 77),
(211, 14110492243, 1001),
(212, 14110508163, 1002);

-- --------------------------------------------------------

--
-- Structure de la table `heure_depart`
--

CREATE TABLE IF NOT EXISTS `heure_depart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(10) NOT NULL,
  `heure` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `heure_depart`
--

INSERT INTO `heure_depart` (`id`, `course`, `heure`) VALUES
(1, 'poussins', 1411053006),
(2, 'benjamins', 1411053006),
(3, 'minimes', 1411045806),
(4, '15', 1411053006),
(5, '7.5', 1411053006);

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

CREATE TABLE IF NOT EXISTS `inscription` (
  `id` bigint(20) NOT NULL,
  `nom` varchar(200) NOT NULL,
  `prenom` varchar(200) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `naissance` varchar(10) NOT NULL,
  `course` varchar(10) NOT NULL,
  `certificat` varchar(500) NOT NULL,
  `paiement` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `inscription`
--

INSERT INTO `inscription` (`id`, `nom`, `prenom`, `sexe`, `naissance`, `course`, `certificat`, `paiement`) VALUES
(1, 'Lebourg', 'Pilar', 'femme', '14/08/1962', '7.5', 'non', 'oui'),
(2, 'Kestenaar', 'Charles', 'homme', '05/04/1961', '7.5', 'non', 'oui'),
(3, 'beuzelin', 'arnaud', 'homme', '07/01/1978', '15', 'oui', 'oui'),
(4, 'Morin', 'guillaume', 'homme', '02/03/1994', '15', 'oui', 'oui'),
(5, 'MAILLARD', 'MEHDI', 'homme', '04/08/1981', '15', 'non', 'oui'),
(6, 'LOUE', 'CEDRICK', 'homme', '06/02/1976', '15', 'oui', 'oui'),
(7, 'LISET ', 'Guillaume', 'homme', '14/08/1997', '15', 'oui', 'oui'),
(8, 'Fadili', 'mehdi', 'homme', '22/05/1994', '15', 'oui', 'oui'),
(9, 'Duval', 'Catherine', 'femme', '20/03/1973', '7.5', 'oui', 'oui'),
(10, 'QUINQUIS', 'DAVID', 'homme', '13/10/1970', '15', 'oui', 'oui'),
(11, 'locard', 'frederic', 'homme', '13/03/1968', '15', 'oui', 'oui'),
(12, 'Deledalle', 'Benjamin', 'homme', '26/02/1979', '15', 'oui', 'oui'),
(13, 'RIVOALEN', 'ERIC', 'homme', '12/01/1964', '7.5', 'oui', 'oui'),
(14, 'BLARD', 'THOMAS', 'homme', '16/03/1971', '15', 'oui', 'oui'),
(15, 'LECLER', 'GREGORY', 'homme', '16/05/1977', '15', 'oui', 'oui'),
(16, 'Foucard', 'sabine', 'femme', '12/01/69', '15', 'non', 'oui'),
(17, 'Damien', 'Auzou', 'homme', '21/08/1976', '15', 'non', 'oui'),
(18, 'FRUSQUE', 'KARINE', 'femme', '21/09/1977', '15', 'oui', 'oui'),
(19, 'POMPILI', 'Guillaume', 'homme', '30/05/1984', '7.5', 'oui', 'oui'),
(20, 'Mignot', 'Stephane', 'homme', '26/11/1976', '15', 'oui', 'oui'),
(21, 'PERRAULT', 'Frederic', 'homme', '07/08/1973', '15', 'non', 'oui'),
(22, 'MARECHAL', 'Isabelle', 'femme', '18/05/1975', '15', 'oui', 'oui'),
(23, 'FOULDRIN', 'Thomas', 'homme', '30/08/1979', '15', 'oui', 'oui'),
(24, 'delannoy', 'matthieu', 'homme', '29/01/1979', '15', 'oui', 'oui'),
(25, 'thierry', 'gregory', 'homme', '30/12/1980', '15', 'non', 'oui'),
(26, 'quicray', 'nathalie', 'femme', '06031973', '15', 'oui', 'oui'),
(27, 'sannier', 'sylvain', 'homme', '29/07/1974', '15', 'oui', 'oui'),
(28, 'LEMESRE', 'david', 'homme', '29/01/1975', '15', 'oui', 'oui'),
(29, 'biet', 'melisande', 'femme', '22/01/1981', '15', 'non', 'oui'),
(30, 'FISSON', 'Benoit', 'homme', '17/02/1975', '15', 'oui', 'oui'),
(31, 'JULIEN', 'Pierre', 'homme', '06/05/1975', '15', 'oui', 'oui'),
(32, 'Chevallier', 'Julien', 'homme', '23/04/1989', '15', 'oui', 'oui'),
(33, 'OLIVIER', 'Jacques', 'homme', '01/02/1956', '15', 'non', 'oui'),
(34, 'Evrard', 'Pierre', 'homme', '08/12/1979', '15', 'non', 'oui'),
(35, 'lucas', 'gilles', 'homme', '11/06/1988', '7.5', 'oui', 'oui'),
(36, 'jactel', 'jean-luc', 'homme', '27/02/1976', '15', 'oui', 'oui'),
(37, 'BLONDEL', 'Adeline', 'femme', '03/04/1990', '15', 'oui', 'oui'),
(38, 'LEBIGRE', 'PIERRE', 'homme', '10/05/1972', '7.5', 'oui', 'oui'),
(39, 'ragÃ©', 'jeremie', 'homme', '19.05.1986', '7.5', 'oui', 'oui'),
(40, 'LECLERC', 'Laurent ', 'homme', '26/12/1968', '15', 'oui', 'oui'),
(41, 'Coget', 'Martine', 'femme', '20/08/1970', '7.5', 'oui', 'oui'),
(42, 'Moitry', 'Pierre', 'homme', '15/11/78', '15', 'oui', 'oui'),
(43, 'GREMONT', 'ARNAUD', 'homme', '30/09/1971', '15', 'oui', 'oui'),
(44, 'santos', 'mickael', 'homme', '18/06/1986', '7.5', 'oui', 'oui'),
(46, 'coudray', 'laurent', 'homme', '11/02/1961', '15', 'oui', 'oui'),
(47, 'BRICHLER', 'Jean-Marc', 'homme', '11/05/1966', '15', 'oui', 'oui'),
(48, 'GICQUEL', 'Christine', 'femme', '15/11/1964', '7.5', 'oui', 'oui'),
(49, 'Lochereau', 'Romain', 'homme', '09/08/1992', '7.5', 'oui', 'oui'),
(50, 'DuchÃªne', 'Emilien', 'homme', '02/06/1993', '7.5', 'oui', 'oui'),
(51, 'CAVOLEAU-GICQUEL', 'MÃ©lanie', 'femme', '08/05/1986', '7.5', 'oui', 'oui'),
(52, 'Le Pape', 'Yannick', 'homme', '04-10-1969', '15', 'oui', 'oui'),
(53, 'Le Pape', 'Julie', 'femme', '05-06-1974', '15', 'oui', 'oui'),
(54, 'Boudard', 'Delphine', 'femme', '24-04-1974', '15', 'oui', 'oui'),
(55, 'FLEURY', 'Guillaume', 'homme', '09/02/1986', '15', 'oui', 'oui'),
(56, 'BOIVIN', 'Christophe', 'homme', '25/05/1979', '15', 'oui', 'oui'),
(57, 'Payel', 'Terry', 'homme', '21/07/1983', '15', 'oui', 'oui'),
(58, 'LEMARCHAND', 'ElisÃ©e', 'homme', '26/11/1971', '15', 'oui', 'oui'),
(59, 'ROUSIERES', 'OLivier', 'homme', '15/10/1966', '15', 'oui', 'oui'),
(60, 'mirabel', 'ludovic', 'homme', '08011976', '15', 'oui', 'oui'),
(61, 'Galliot', 'Joseph', 'homme', '27/10/1976', '15', 'oui', 'oui'),
(62, 'GATHERON', 'THIBAULT', 'homme', '17/06/1989', '15', 'oui', 'oui'),
(63, 'beuzelin', 'peggy', 'femme', '30/12/1977', '7.5', 'oui', 'oui'),
(64, 'Anicot', 'Samantha', 'femme', '26/11/1972', '7.5', 'non', 'oui'),
(65, 'bossart', 'david', 'homme', '17/04/1979', '15', 'non', 'oui'),
(66, 'Barthe', 'Gilles', 'homme', '25/04/1972', '15', 'non', 'oui'),
(67, 'MAILLOT', 'Sophie ', 'femme', '13-10-1978', '15', 'oui', 'oui'),
(68, 'BrÃ©maud', 'Julien', 'homme', '15/08/1981', '15', 'non', 'oui'),
(69, 'FERRET', 'Morgane', 'femme', '25/09/75', '7.5', 'oui', 'oui'),
(70, 'FOURDRINIER', 'Lucie', 'femme', '18/09/1982', '7.5', 'oui', 'oui'),
(71, 'bacon', 'alain', 'homme', '05.03.1966', '15', 'oui', 'oui'),
(72, 'LEMARCHAND', 'Franck', 'homme', '15/10/61', '7.5', 'oui', 'oui'),
(73, 'GARREAU', 'Vincent', 'homme', '01/12/1988', '7.5', 'oui', 'oui'),
(74, 'leroy', 'fanny', 'femme', '15/10/1973', '7.5', 'oui', 'oui'),
(75, 'FELIX', 'Guillaume', 'homme', '02/03/1986', '15', 'oui', 'oui'),
(76, 'leroy', 'samuel', 'homme', '25/08/2014', '15', 'non', 'oui'),
(77, 'galle', 'jacky', 'homme', '17/10/1956', '15', 'non', 'oui'),
(78, 'LEMESRE', 'celine', 'femme', '05/03/1976', '7.5', 'oui', 'oui'),
(79, 'CLÃ‰ROUX', 'Gilles', 'homme', '02/05/1955', '15', 'oui', 'oui'),
(80, 'CLÃ‰ROUX', 'Gilles', 'homme', '02/05/1955', '15', 'oui', 'oui'),
(81, 'GALLARD', 'YANNICK', 'homme', '04/04/1962', '15', 'oui', 'oui'),
(82, 'guery', 'alexis', 'homme', '17/05/1987', '15', 'oui', 'oui'),
(83, 'MIGRAINE', 'Christophe', 'homme', '20/02/1983', '7.5', 'oui', 'oui'),
(84, 'VAN DEN BRINK', 'Barbara', 'femme', '12/06/1968', '15', 'oui', 'oui'),
(85, 'cordier', 'yohan', 'homme', '26/04/1979', '15', 'oui', 'oui'),
(86, 'DONO', 'Jonathan', 'homme', '27/04/1979', '15', 'oui', 'oui'),
(87, 'Motta', 'Matthieu', 'homme', '13/03/1982', '15', 'oui', 'oui'),
(88, 'DUVAL', 'Etienne', 'homme', '29/04/1976', '15', 'oui', 'oui'),
(89, 'potier', 'margareth', 'femme', '22/01/1973', '7.5', 'non', 'oui'),
(90, 'potier', 'arnaud', 'homme', '03/06/1976', '7.5', 'non', 'oui'),
(91, 'jessus', 'laurent', 'homme', '21/10/1966', '15', 'oui', 'oui'),
(92, 'rallu', 'virginie', 'femme', '16/04/75', '15', 'non', 'oui'),
(93, 'GENEST', 'ERICK', 'homme', '17/02/1971', '15', 'non', 'oui'),
(94, 'BARR', 'LounÃ¨s', 'homme', '14/09/1974', '15', 'oui', 'oui'),
(95, 'Chesnel', 'Julien', 'homme', '27/10/1982', '15', 'oui', 'oui'),
(96, 'Zolli', 'Emmanuel', 'homme', '20/12/1973', '15', 'oui', 'oui'),
(97, 'Geffroy ', 'Amandine', 'femme', '15/02/1981', '7.5', 'oui', 'oui'),
(98, 'GALLIOT', 'CÃ©lia', 'femme', '12/07/1976', '7.5', 'non', 'oui'),
(99, 'FRUCHAUD', 'PASCAL', 'homme', '23/11/1970', '15', 'non', 'oui'),
(100, 'mignot', 'Christophe', 'homme', '11/02/1967', '15', 'oui', 'oui'),
(101, 'Ouvry', 'StÃ©phane', 'homme', '04091967', '7.5', 'non', 'oui'),
(102, 'bachelet', 'adele', 'femme', '31/12/1985', '7.5', 'oui', 'oui'),
(103, 'Hauguel', 'Camille', 'femme', '09/04/1993', '7.5', 'oui', 'oui'),
(104, 'FLAMANT', 'Thierry', 'homme', '17/08/1966', '7.5', 'oui', 'oui'),
(105, 'mezeray', 'jerome', 'homme', '03/08/1972', '15', 'non', 'oui'),
(106, 'chouin', 'estelle', 'femme', '15/10/1977', '7.5', 'non', 'oui'),
(107, 'delarue', 'guillaume', 'homme', '20/07/1977', '15', 'oui', 'oui'),
(108, 'Tonnetot', 'Lise', 'femme', '28/06/1953', '15', 'non', 'oui'),
(109, 'COUSIN', 'Sabine', 'femme', '22/09/1974', '7.5', 'oui', 'oui'),
(110, 'DUVAL', 'OLIVIER', 'homme', '16/04/1957', '7.5', 'oui', 'oui'),
(111, 'DUSSAUX', 'BenoÃ®t', 'homme', '04/12/1967', '7.5', 'oui', 'oui'),
(112, 'anquetil', 'laurent', 'homme', '29/11/1964', '15', 'oui', 'oui'),
(113, 'MICHAUX', 'DIDIER', 'homme', '29/12/1969', '15', 'oui', 'oui'),
(114, 'Banos', 'Denis', 'homme', '22/08/68', '7.5', 'non', 'oui'),
(115, 'MASSE', 'VIRGINIE', 'femme', '28/11/1978', '7.5', 'oui', 'oui'),
(116, 'AGEZ', 'CAROLINE', 'femme', '12/10/1974', '7.5', 'oui', 'oui'),
(117, 'DUVAL', 'Altaye', 'homme', '09/02/1997', '7.5', 'oui', 'oui'),
(118, 'LEGRAND', 'didier', 'homme', '19/10/1957', '15', 'oui', 'oui'),
(119, 'FOLLEA', 'Guillaume', 'homme', '12/05/1968', '15', 'oui', 'oui'),
(120, 'massot', 'valerie', 'femme', '14/04/1964', '15', 'non', 'oui'),
(121, 'thierry', 'bruno', 'homme', '09/091966', '15', 'non', 'oui'),
(122, 'DONO', 'Emmanuel', 'homme', '28/04/1983', '15', 'oui', 'oui'),
(123, 'Delacroix', 'Christine', 'femme', '10/08/1964', '15', 'oui', 'oui'),
(124, 'MAUROVIC', 'Yvan', 'homme', '31/03/1975', '15', 'oui', 'oui'),
(125, 'GARCIA', 'Florian', 'homme', '17/09/89', '15', 'non', 'oui'),
(126, 'BOUARICHE', 'Adel', 'homme', '08/03/90', '15', 'non', 'oui'),
(127, 'VIEVARD', 'Christian', 'homme', '14/111954', '15', 'oui', 'oui'),
(128, 'Glorieux', 'Pierrick', 'homme', '28/05/1985', '15', 'oui', 'oui'),
(129, 'LEVASSEUR', 'PHILIPPE', 'homme', '16/03/1952', '7.5', 'oui', 'oui'),
(130, 'LAVENU', 'Olivier', 'homme', '11 MARS 19', '15', 'non', 'oui'),
(131, 'rose', 'emmanuel', 'homme', '04/01/1972', '15', 'non', 'oui'),
(132, 'SERREAU', 'jean', 'homme', '14/07/1964', '15', 'oui', 'oui'),
(133, 'JEANNE', 'Arthur', 'homme', '20/02/1992', '15', 'oui', 'oui'),
(134, 'Vincent', 'CÃ©dric', 'homme', '04/06/1971', '15', 'oui', 'oui'),
(135, 'MIGRAINE', 'StÃ©phanie', 'femme', '03/04/1984', '7.5', 'oui', 'oui'),
(136, 'simonte', 'nathanael', 'homme', '19/09/1987', '15', 'non', 'oui'),
(137, 'Lemonnier', 'laurent', 'homme', '20/03/1970', '15', 'oui', 'oui'),
(138, 'Hebert', 'Jean Philippe', 'homme', '14/11/1982', '7.5', 'non', 'oui'),
(139, 'de sagazan', 'constance', 'femme', '08/06/1986', '7.5', 'non', 'oui'),
(140, 'todisco', 'jenifer', 'femme', '01/07/1983', '7.5', 'oui', 'oui'),
(141, 'mercier', 'jean philippe', 'homme', '24/11/55', '15', 'oui', 'oui'),
(142, 'Canchy', 'David', 'homme', '23/05/1971', '7.5', 'oui', 'oui'),
(143, 'GALLE', 'Vincent', 'homme', '02/05/1985', '7.5', 'oui', 'oui'),
(144, 'PETIT', 'LUC', 'homme', '16/12/1969', '7.5', 'non', 'oui'),
(145, 'Lahaye', 'Laurence', 'femme', '22 05 1970', '7.5', 'non', 'oui'),
(146, 'saillard', 'tony', 'homme', '31/01/1994', '7.5', 'oui', 'oui'),
(147, 'TOUSSAINT', 'CHRISTOPHE', 'homme', '05/03/1968', '15', 'oui', 'oui'),
(148, 'BOUTIGNY', 'DANIEL', 'homme', '27/11/1953', '15', 'oui', 'oui'),
(149, 'RICHARD', 'DIDIER', 'homme', '26:03:1959', '15', 'non', 'oui'),
(150, 'VANRELL', 'ARNAUD', 'homme', '15/09/1982', '7.5', 'oui', 'oui'),
(151, 'Oliva', 'Louca', 'homme', '12/07/2000', '15', 'oui', 'oui'),
(152, 'Zeni-Guido', 'Bertrand', 'homme', '16/08/1963', '15', 'oui', 'oui'),
(153, 'VINCENT', 'THIERRY', 'homme', '16/02/1963', '15', 'oui', 'oui'),
(154, 'LE PALLEC', 'Jean-SÃ©bastien', 'homme', '12 dÃ©cemb', '15', 'non', 'oui'),
(155, 'cattaneo', 'stÃ©fan', 'homme', '16/11/1973', '15', 'oui', 'oui'),
(156, 'LE PALLEC', 'Axel', 'homme', '24 aoÃ»t 1', '7.5', 'non', 'oui'),
(157, 'Mesureux', 'Vincent ', 'homme', '14/02/1970', '15', 'non', 'oui'),
(158, 'Roquin', 'GrÃ©gory', 'homme', '08/04/1977', '15', 'oui', 'oui'),
(159, 'PHILIPPE', 'Christelle', 'femme', '13/02/1977', '7.5', 'oui', 'oui'),
(160, 'YON', 'JÃ©rome', 'homme', '30/06/1977', '15', 'oui', 'oui'),
(161, 'CLEROUX', 'ARMAND', 'homme', '28/09/1989', '15', 'oui', 'oui'),
(162, 'DEMINIER', 'JEROME', 'homme', '07/05/1971', '7.5', 'non', 'oui'),
(163, 'LAHAYE', 'Jean-FranÃ§ois', 'homme', '16/08/1964', '15', 'non', 'oui'),
(164, 'dillard', 'claire', 'femme', '25/07/1978', '7.5', 'oui', 'oui'),
(165, 'sinca', 'alexandra', 'femme', '26/03/1983', '7.5', 'non', 'oui'),
(166, 'Hyvart', 'Maxime', 'homme', '11/09/1985', '7.5', 'non', 'oui'),
(167, 'VANRELL', 'Adeline', 'femme', '15/09/1984', '7.5', 'oui', 'oui'),
(168, 'Mauger', 'SÃ©bastien', 'homme', '26/03/1973', '15', 'non', 'oui'),
(169, 'foutrel', 'ludovic', 'homme', '16/01/1986', '15', 'oui', 'oui'),
(170, 'Bance', 'Tristan', 'homme', '19/12/1995', '7.5', 'oui', 'oui'),
(171, 'Illand', 'Sebastien', 'homme', '04/11/1970', '7.5', 'oui', 'oui'),
(172, 'Illand', 'Corentin', 'homme', '07/08/1995', '7.5', 'oui', 'oui'),
(173, 'Bance', 'Eric', 'homme', '29/08/1968', '15', 'oui', 'oui'),
(174, 'morel', 'laurent', 'homme', '09051959', '15', 'non', 'oui'),
(175, 'boismard', 'jonathan', 'homme', '02/02/1987', '7.5', 'non', 'oui'),
(176, 'HOUDEVILLE', 'Fabian', 'homme', '17/03/1968', '15', 'oui', 'oui'),
(177, 'Robert de Saint Vincent', 'Marguerite', 'femme', '04/07/1993', '7.5', 'oui', 'oui'),
(178, 'Vauloup', 'Matthieu', 'homme', '15/05/1989', '7.5', 'oui', 'oui'),
(179, 'PIOT', 'Coralie', 'femme', '6/10/93', '7.5', 'oui', 'oui'),
(180, 'BEDEL', 'Claire', 'femme', '13/02/1965', '15', 'oui', 'oui'),
(181, 'BRIAUX', 'ALBANE', 'femme', '10/10/1896', '7.5', 'oui', 'oui'),
(182, 'vandecandelaere', 'Albane', 'femme', '12/04/1993', '7.5', 'oui', 'oui'),
(183, 'Arcillon', 'Romain', 'homme', '09/03/1989', '15', 'oui', 'oui'),
(185, 'FEUILLOLEY', 'DELPHINE', 'femme', '12/07/1980', '7.5', 'oui', 'oui'),
(186, 'Dumontier ', 'Alexandra ', 'femme', '30/06/1979', '15', 'non', 'oui'),
(187, 'CARPENTIER', 'RÃ©gis', 'homme', '16/10/1964', '15', 'oui', 'oui'),
(188, 'ZOLLI', 'SOPHIE', 'femme', '29/09/1975', '15', 'non', 'oui'),
(189, 'DALONGEVILLE', 'ALBERIK', 'homme', '18/01/1970', '15', 'non', 'oui'),
(190, 'viot', 'pierre', 'homme', '25/041989', '15', 'oui', 'oui'),
(191, 'BOYER', 'FABIEN', 'homme', '13/03/1980', '7.5', 'oui', 'oui'),
(192, 'PETIT', 'BENOIT', 'homme', '09/12/1971', '15', 'oui', 'oui'),
(193, 'BURE', 'Gaylord', 'homme', '20/10/1980', '7.5', 'oui', 'oui'),
(194, 'DELAPORTE', 'LAURENT', 'homme', '13/03/1969', '15', 'oui', 'oui'),
(195, 'DELAPORTE', 'sophie', 'femme', '05/09/1972', '7.5', 'oui', 'oui'),
(196, 'DUHAMEL', 'dominique', 'homme', '17/12/1957', '7.5', 'oui', 'oui'),
(197, 'CAPUANO', 'LOUIS', 'homme', '22/06/1965', '15', 'oui', 'oui'),
(198, 'LEMERCIER', 'StÃ©phane', 'homme', '24/06/1966', '15', 'oui', 'oui'),
(199, 'houdeville', 'ingrid', 'femme', '07/10/1974', '15', 'non', 'oui'),
(200, 'brunet', 'viviane', 'femme', '17/06/1970', '7.5', 'non', 'oui'),
(201, 'brunet', 'dominique', 'homme', '06/09/1968', '7.5', 'non', 'oui'),
(202, 'Petit', 'Sandra', 'femme', '19/01/1972', '7.5', 'oui', 'oui'),
(203, 'Delamotte', 'Laurent', 'homme', '23/02/1966', '15', 'oui', 'oui'),
(204, 'Delamotte', 'Nadine', 'femme', '05/01/1967', '7.5', 'oui', 'oui'),
(205, 'paillard', 'christophe', 'homme', '30/09/1969', '7.5', 'oui', 'oui'),
(14110492243, 'LEBOURG', 'NICOLAS', 'homme', '25/02/1999', 'minimes', 'oui', 'non'),
(14110508163, 'LESUEUR', 'ANAIS', 'femme', '25/02/1999', 'minimes', 'oui', 'non');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
