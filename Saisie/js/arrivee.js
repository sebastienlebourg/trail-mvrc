$(document).ready(function() {
	$('input#saisi_dossard').keypress(function(e){
		if( e.which == 13 ){
			var course = $(this).attr("data-course");
			var dossard = $(this).val();
			
			var xhr = getXhr();
			// On défini ce qu'on va faire quand on aura la réponse
			xhr.onreadystatechange = function(){
				// On ne fait quelque chose que si on a tout reçu et que le serveur est ok
				if(xhr.readyState == 4 && xhr.status == 200){
					liste_arrivant = xhr.responseText;
					// On se sert de innerHTML pour rajouter les options a la liste
					document.getElementById('liste_arrivants').innerHTML = liste_arrivant;
					$('input#saisi_dossard').val("");
				}
			}

			// Ici on va voir comment faire du post
			xhr.open("POST","saisi_arrivee.php",true);
			// ne pas oublier ça pour le post
			xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			// ne pas oublier de poster les arguments
			xhr.send("course="+course+"&dossard="+dossard);
				
		}
	});
	
	function getXhr(){
        var xhr = null; 
		if(window.XMLHttpRequest) // Firefox et autres
		   xhr = new XMLHttpRequest(); 
		else if(window.ActiveXObject){ // Internet Explorer 
		   try {
					xhr = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					xhr = new ActiveXObject("Microsoft.XMLHTTP");
				}
		}
		else { // XMLHttpRequest non supporté par le navigateur 
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
		   xhr = false; 
		} 
		return xhr;
	}
	
});