<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Les 15 km de Montigny</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header" style="text-align:center; width:100%;">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span style="text-align:center; width:100%; text-transform:uppercase;" class="navbar-brand">Les 15 kilomètres de Montigny</span></a>
		</div>
      </div>
    </div>
    

	<?php include('menu.html'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">Inscriptions des <?php print $_GET['course']; ?></h1>
			<ul class="breadcrumb">
				<li><a href="index.p">Accueil</a> </li>
				<li class="active">Inscriptions</li>
				<li class="active">Inscriptions des <?php print $_GET['course']; ?></li>
			</ul>
        </div>
                <div class="main-content">
            
<div class="btn-toolbar list-toolbar">
    <a href="add_inscription.php?course=<?php print $_GET['course']; ?>" style="text-decoration:none; color:#FFFFFF;"><button class="btn btn-primary"><i class="fa fa-plus"></i> Nouvelle inscription</button></a>
    <a target="_blank" href="dossards-relais.php?course=<?php print $_GET['course']; ?>" style="text-decoration:none; color:#000000;"><button class="btn btn-default">Retrait des dossards</button></a>
    <form action="inscriptions.php?course=<?php echo $_GET['course'] . '&'; ?>action=search" method="post" style="float:right;">
		<input type="text" name="search">
		<input type="submit" class="btn btn-default" value="Rechercher">
	</form>
  <div class="btn-group">
  </div>
</div>
<table class="table">
  <thead>
    <tr>
      <!--<th>Dossard</th>-->
      <th>Courreur 1</th>
      <th>Courreur 2</th>
      <th>Naissance</th>
      <th>Sexe</th>
      <th>Catégorie</th>
      <th>Modifier</th>
    </tr>
  </thead>
  <tbody>
    <?php
		// $sql_dossard = 'SELECT debut_dossard from attribution_dossard WHERE course LIKE "'.$_GET["course"].'"';
        
    // include('connexion_database.php');
    // $req_dossard = mysql_query($sql_dossard) or die('Erreur SQL !<br>'.$sql_dossard.'<br>'.mysql_error());
     // $dossard = '500';
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    // while($data_dossard = mysql_fetch_assoc($req_dossard))
    // {
      // $dossard = $data_dossard['debut_dossard'];
    // }

    if(isset($_GET['action'])){
      $sql = 'SELECT i.id, nom, prenom, naissance, sexe, course, relais 
              FROM inscription i
              WHERE course = '.$_GET["course"] . ' 
              AND (nom LIKE "%'.$_POST["search"].'%" OR prenom LIKE "%'.$_POST["search"].'%")
			  AND (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }elseif(isset($_GET['course'])){
      $sql = 'SELECT i.id, nom, prenom, naissance, sexe, course, relais  
        FROM inscription i
        WHERE course = "'.$_GET["course"] . '"
		AND (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }else{
      $sql = 'SELECT i.id, nom, prenom, naissance, sexe, course, relais  
              FROM inscription i
			  WHERE (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }

    include('connexion_database.php');
    $req = $conn->query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.$conn->mysqli_error());
    
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    while($data = $req->fetch_assoc())
    {	
			
			 // $sql_dossard = "INSERT INTO `dossards` (`id_coureur`, `dossard`) VALUES	(".$data['id'].", ".$dossard.")";
			 // $requete = mysql_query($sql_dossard) or die('Erreur SQL !<br>'.$sql_dossard.'<br>'.mysql_error());
			
			
			$age =  date("Y") - substr($data['naissance'], -4, 4);
			
			
			if($age < 12){
				$categorie = "POU" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 14){
				$categorie = "BEN" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 16){
				$categorie = "MIN" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 18){
				$categorie = "CAD" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 20){
				$categorie = "JUN" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 22){
				$categorie = "ESP" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 40){
				$categorie = "SE" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 50){
				$categorie = "V1" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 60){
				$categorie = "V2" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 70){
				$categorie = "V3" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}elseif($age < 80){
				$categorie = "V4" . ' ' . strtoupper(substr($data['sexe'], 0, 1));
			}else{
				$categorie = "ERROR";
			}
			
      $data_relais = explode("|", $data['relais']);

		?>
			<tr>
			  <!--<td><?php print strtoupper($data['dossard']); ?></td>-->
			  <td><?php print strtoupper($data['nom']); ?> <?php print strtoupper($data['prenom']); ?></td>
			  <td><?php print strtoupper($data_relais['0']); ?> <?php print strtoupper($data_relais['1']); ?></td>
			  <td><?php print strtoupper($data['naissance']); ?></td>
			  <td><?php print strtoupper($data['sexe']); ?></td>
			  <td><?php print $categorie; ?></td>
			  <td><a href="add_inscription.php?edit=<?php print $data['id']; ?>"><i class="fa fa-pencil"></i></a></td>
			</tr>
		<?php		
      // $dossard = $dossard + 1;
    }
	?>
  </tbody>
</table>

            <footer>
                <hr>
				<p class="pull-right">Les 15 kilomètres de Montigny</p>
                <p>© 2015 <a href="http://slebourg.fr" target="_blank">S.Lebourg</a></p>
            </footer>
        </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
