<?php
    include("refresh.php");
?>

<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Trail de Noël 2019</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header" style="text-align:center; width:100%;">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span style="text-align:center; width:100%; text-transform:uppercase;" class="navbar-brand">Trail de Noël 2019</span></a>
		</div>
      </div>
    </div>
    

	<?php include('menu.html'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">Dashboard</h1>
			<ul class="breadcrumb">
				<li><a href="index.p">Accueil</a> </li>
				<li class="active">Dashboard</li>
			</ul>
        </div>
        <div class="main-content">
            
				<?php
					$sql = 'SELECT count(id) as nb_total
							  FROM inscription i where (paiement = "valider"  OR paiement = "non")';
							  
					$sql_11 = 'SELECT count(id) as nb_total
							  FROM inscription i
							  WHERE course = "11" and (paiement = "valider"  OR paiement = "non")';
                              
                    $sql_21 = 'SELECT count(id) as nb_total
                              FROM inscription i
                              WHERE course = "21" and (paiement = "valider"  OR paiement = "non")';
                              
					include('connexion_database.php');
					$req = $conn->prepare($sql);
					$req->execute();
					
					// on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data = $req->fetch(PDO::FETCH_ASSOC))
					{	
						$nb_total = $data['nb_total'];
					}
					
					$req_11 = $conn->prepare($sql_11);
					$req_11->execute();
    
					// on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data_11 = $req_11->fetch(PDO::FETCH_ASSOC))
					{	
						$nb_11= $data_11['nb_total'];
					}					
					
					$req_21 = $conn->prepare($sql_21);
					$req_21->execute();
    
                    // on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data_21 = $req_21->fetch(PDO::FETCH_ASSOC))
					{	
						$nb_21 = $data_21['nb_total'];
					}
                    	
				?>
    <div class="panel panel-default">
        <a href="#page-stats" class="panel-heading" data-toggle="collapse">Inscriptions total: <?php print $nb_total; ?></a>
        <div id="page-stats" class="panel-collapse panel-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="knob-container">
                                <input class="knob" data-width="200" data-min="0" data-max="<?php print $nb_total; ?>" data-displayPrevious="true" value="<?php print $nb_11; ?>" data-fgColor="#92A3C2" data-readOnly=true;>
                                <h3 class="text-muted text-center">Inscriptions 11 km</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="knob-container">
                                <input class="knob" data-width="200" data-min="0" data-max="<?php print $nb_total; ?>" data-displayPrevious="true" value="<?php print $nb_21; ?>" data-fgColor="#92A3C2" data-readOnly=true;>
                                <h3 class="text-muted text-center">Inscriptions 21 km</h3>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
	
	    <div class="panel panel-default">
        <a href="#page-origine" class="panel-heading" data-toggle="collapse">Origines des inscriptions</a>
        <div id="page-origine" class="panel-collapse panel-body collapse">
				<?php

					$sql_web = 'SELECT count(id) as nb_total
							  FROM inscription i
							  WHERE (paiement = "valider"  OR paiement = "non") and type_paiement = "web"';
							  
					$sql_papier = 'SELECT count(id) as nb_total
							  FROM inscription i
							  WHERE (paiement = "valider"  OR paiement = "non") and type_paiement != "web"';
							  		
					$req_web = $conn->prepare($sql_web);
					$req_web->execute();
    
					
					// on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data_web = $req_web->fetch(PDO::FETCH_ASSOC))
					{	
						$nb_web = $data_web['nb_total'];
					}
					$req_papier = $conn->prepare($sql_papier);
					$req_papier->execute();
					
					// on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data_papier = $req_papier->fetch(PDO::FETCH_ASSOC))
					{	
						$nb_papier = $data_papier['nb_total'];
					}
					
					
				?>

                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="knob-container">
                                <input class="knob" data-width="200" data-min="0" data-max="<?php print $nb_total; ?>" data-displayPrevious="true" value="<?php print $nb_total; ?>" data-fgColor="#92A3C2" data-readOnly=true;>
                                <h3 class="text-muted text-center">TOTAL</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="knob-container">
                                <input class="knob" data-width="200" data-min="0" data-max="<?php print $nb_total; ?>" data-displayPrevious="true" value="<?php print $nb_web; ?>" data-fgColor="#92A3C2" data-readOnly=true;>
                                <h3 class="text-muted text-center">WEB</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="knob-container">
                                <input class="knob" data-width="200" data-min="0" data-max="<?php print $nb_total; ?>" data-displayPrevious="true" value="<?php print $nb_papier; ?>" data-fgColor="#92A3C2" data-readOnly=true;>
                                <h3 class="text-muted text-center">PAPIER</h3>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading no-collapse">Derniers Inscrits</div>
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nom Prénom</th>
                  <th>Date de naissance</th>
                  <th>Sexe</th>
                  <th>Course</th>
                </tr>
              </thead>
              <tbody>
			   <?php
					$sql = 'SELECT i.id, nom, prenom, naissance, sexe, course 
							  FROM inscription i
							  ORDER BY i.id DESC 
							  LIMIT 0,5';

                    include('connexion_database.php');
                    
					$req = $conn->prepare($sql);
					$req->execute();
					
					// on fait une boucle qui va faire un tour pour chaque enregistrement
                    while($data = $req->fetch(PDO::FETCH_ASSOC))
					{	
						?>
						<tr>
						  <td><?php print $data['nom'] . " " .  $data['prenom']; ?></td>
						  <td><?php print $data['naissance']; ?></td>
						  <td><?php print $data['sexe']; ?></td>
						  <td><?php print $data['course']; ?></td>
						</tr>
						<?php
					}
				?>
              </tbody>
            </table>
        </div>
    </div>
</div>

            <footer>
                <hr>
				<p class="pull-right">Trail de Noël 2019</p>
                <p>© 2019 <a href="#" target="_blank">S.Lebourg</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
