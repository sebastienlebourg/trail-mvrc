<?php
require('fpdf.php');

class PDF extends FPDF
{
// Chargement des données
function LoadData()
{
	
	$sql = 'SELECT dossard, nom, prenom, naissance, sexe, certificat, paiement, type_paiement
		FROM inscription i, dossards d
		WHERE i.id = d.id_coureur 
		AND course = "'.$_GET['course'].'"
		ORDER BY nom';    
	include('connexion_database.php');
	$req = $conn->query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.$conn->mysqli_error());
	
	// on fait une boucle qui va faire un tour pour chaque enregistrement
	while($data = $req->fetch_assoc())
	{	
		$datas[] = $data;
	}
		
	return $datas;
	

}

// Tableau coloré
function FancyTable($header, $data)
{
	// Couleurs, épaisseur du trait et police grasse
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetDrawColor(128,0,0);
	$this->SetLineWidth(.3);
	$this->SetFont('','B');
	// En-tête
	$w = array(20, 55, 55, 30, 20, 20, 20, 30, 30);
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],8,$header[$i],1,0,'C',true);
	$this->Ln();
	// Restauration des couleurs et de la police
	$this->SetFillColor(255,0,0);
	$this->SetTextColor(0);
	$this->SetFont('');
	// Données
	$fill = false;
	foreach($data as $row)
	{
		$fill = false;
		$this->SetTextColor(0);
		
		if($row['certificat'] == "non" || $row['certificat'] == "" || $row['paiement'] == "non" || $row['naissance'] == "" || $row['sexe'] == ""){
			$fill = true;
			$this->SetTextColor(255);
		}

		if($row['certificat'] == "non" || $row['certificat'] == ""){
			$row['certificat'] = "non";
		}else{
			$row['certificat'] = "oui";
		}
		if($row['paiement'] == "valider"){
			$row['paiement'] = "OK";
		}else{
			$row['paiement'] = "AUCUN";
		}
		
		//var_dump($row); die;
		$this->Cell($w[0],7,strtoupper(utf8_decode($row['dossard'])), 1,0,'C',$fill);
		$this->Cell($w[1],7,strtoupper(utf8_decode($row['nom'])), 1,0,'C',$fill);
		$this->Cell($w[2],7,strtoupper(utf8_decode($row['prenom'])), 1,0,'C',$fill);
		$this->Cell($w[3],7,strtoupper(utf8_decode($row['naissance'])), 1,0,'C',$fill);
		$this->Cell($w[4],7,strtoupper(utf8_decode($row['sexe'])), 1,0,'C',$fill);
		$this->Cell($w[5],7,strtoupper(utf8_decode($row['certificat'])), 1,0,'C',$fill);
		$this->Cell($w[6],7,strtoupper(utf8_decode($row['paiement'])), 1,0,'C',$fill);
		$this->Cell($w[7],7,strtoupper(utf8_decode($row['type_paiement'])), 1,0,'C',$fill);
		$this->Cell($w[8],7,'', 1,0,'C',$fill);

		$this->Ln();
	}
	// Trait de terminaison
	$this->Cell(array_sum($w),0,'','T');
}
}

$pdf = new PDF();
// Titres des colonnes
$header = array('Dossard', 'Nom', 'Prenom', 'Naissance', 'Sexe', 'Certificat', 'Paiement', 'Type Paiement', 'Remarque');
// Chargement des données
$datas = $pdf->LoadData();
$pdf->SetFont('Arial','',10);
$pdf->AddPage('L');
$pdf->FancyTable($header,$datas);
$pdf->Output();
?>