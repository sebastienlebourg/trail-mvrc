<?php
    $sql = "SELECT cu.email as email, c.id, customFields, info, eventPeriodId FROM wp_amelia_customer_bookings c, wp_amelia_users cu, wp_amelia_customer_bookings_to_events_periods e WHERE c.id = e.customerBookingId AND c.customerId = cu.id";
    include('connexion_database.php');
    $req = $conn->prepare($sql);
    $req->execute();

    // on fait une boucle qui va faire un tour pour chaque enregistrement
    $inscriptions = array();
    $inscriptions_a_ajouter = array();
    while($data = $req->fetch(PDO::FETCH_ASSOC))
    {
        $inscriptions[] = $data;
    }

    foreach($inscriptions as $data){
        $email = $data["email"];
        $id_web = $data["id"];
        $sql = 'UPDATE inscription SET email = "'.$email.'" WHERE id_web = "'.$id_web.'"';
        
        $req_update = $conn->prepare($sql);
        $req_update->execute();

        $sql = "SELECT count(*) as exist FROM inscription WHERE id_web = " . $data["id"];
        $req = $conn->prepare($sql);
        $req->execute();
        while($dataexist = $req->fetch(PDO::FETCH_ASSOC))
        {	
            $exist = $dataexist['exist'];
            if($exist > 0){
            }else{
                $inscriptions_a_ajouter[] = $data;
            }
        }
    }

    foreach($inscriptions_a_ajouter as $data){
        
        $customFields = json_decode($data["customFields"]);
        $infos = json_decode($data["info"]);
        $lastName = $infos->lastName;
        $firstName = $infos->firstName;
        $email = $data["email"];
        $naissance = $customFields->{"2"}->value;
        $sexe = $customFields->{"3"}->value;
        $course = $data["eventPeriodId"] == "1" ? "11" : "21";
        $tel = $infos->phone;
        $id_web = $data["id"];
        $fichier = "http://www.traildenoel.fr/wp-content/uploads/amelia/files/".$id_web."_".$customFields->{"1"}->value["0"]->fileName;
        
        $sql = "INSERT INTO `inscription` (`nom`, `prenom`, `naissance`, `sexe`, `course`, `certificat`, `paiement`, `email`, `tel`, type_paiement, id_web, fichier) 
    		VALUES	(
                '".$lastName."',
                '".$firstName."',
                '".$naissance."',
                '".$sexe."',
                '".$course."',
                'a verifier',
                'valider',
                '".$email."',
                '".$tel."',
                'web',
                ".$id_web.",
                '".$fichier."'
            )";
        $req = $conn->prepare($sql);
        $req->execute();
    }

?>