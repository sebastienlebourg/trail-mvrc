<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Les 15 km de Montigny</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="js/arrivee.js" type="text/javascript"></script>
    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header" style="text-align:center; width:100%;">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span style="text-align:center; width:100%; text-transform:uppercase;" class="navbar-brand">Les 15 kilomètres de Montigny</span></a>
    </div>
      </div>
    </div>
    

  <?php include('menu.html'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">Arrivée</h1>
      <ul class="breadcrumb">
        <li><a href="index.p">Accueil</a> </li>
        <li class="active">Arrivée <?php print $_GET['course']; ?></li>
      </ul>
        </div>
        <div class="main-content">

<div class="row">
    <div class="col-md-3">
	
		<?php
			if($_GET["course"] == "7.5"){
				$sql = 	'SELECT heure, course
						FROM heure_depart
						WHERE course = "7.5"';
			}elseif($_GET["course"] == "15"){
				$sql = 	'SELECT heure, course
						FROM heure_depart
						WHERE course = "15"';
			}elseif($_GET["course"] == "poussins"){
				$sql = 	'SELECT heure, course
						FROM heure_depart
						WHERE course = "poussins"';
			}elseif($_GET["course"] == "benjamins"){
				$sql = 	'SELECT heure, course
						FROM heure_depart
						WHERE course = "benjamins"';
			}elseif($_GET["course"] == "minimes"){
				$sql = 	'SELECT heure, course
						FROM heure_depart
						WHERE course = "minimes"';
			}
						  
			include('connexion_database.php'); 
    $req = $conn->query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.$conn->mysqli_error());
    
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    while($data = $req->fetch_assoc())
    {	
				$depart = date('H:i:s', $data["heure"]);							  
			}
		?>
	
      <label>Numéro du dossard</label><br/>
      <input id="saisi_dossard" data-depart="<?php print $depart; ?>" data-course="<?php print $_GET['course']; ?>" type="text" name="num_dossard" value="" class="form-control">
    </div>

    <div class="col-md-9">
        <div class="widget">
                    <ul id="liste_arrivants" class="list-group cards">
                        <?php
                            if($_GET["course"] == "7.5"){
								$sql = 'SELECT d.dossard, nom, prenom, naissance, sexe, temps
                                    FROM inscription i, dossards d, arrivee_75 a
                                    WHERE i.id = d.id_coureur
                                    AND a.dossard = d.dossard
									ORDER BY a.id DESC
									LIMIT 0,5';
							}elseif($_GET["course"] == "15"){
								$sql = 'SELECT d.dossard, nom, prenom, naissance, sexe, temps
                                    FROM inscription i, dossards d, arrivee_15 a
                                    WHERE i.id = d.id_coureur
                                    AND a.dossard = d.dossard
									ORDER BY a.id DESC
									LIMIT 0,5';
							}elseif($_GET["course"] == "poussins"){
								$sql = 'SELECT d.dossard, nom, prenom, naissance, sexe, temps
                                    FROM inscription i, dossards d, arrivee_poussins a
                                    WHERE i.id = d.id_coureur
                                    AND a.dossard = d.dossard
									ORDER BY a.id DESC';
							}elseif($_GET["course"] == "benjamins"){
								$sql = 'SELECT d.dossard, nom, prenom, naissance, sexe, temps
                                    FROM inscription i, dossards d, arrivee_benjamins a
                                    WHERE i.id = d.id_coureur
                                    AND a.dossard = d.dossard
									ORDER BY a.id DESC
									LIMIT 0,5';
							}elseif($_GET["course"] == "minimes"){
								$sql = 'SELECT d.dossard, nom, prenom, naissance, sexe, temps
                                    FROM inscription i, dossards d, arrivee_minimes a
                                    WHERE i.id = d.id_coureur
                                    AND a.dossard = d.dossard
									ORDER BY a.id DESC
									LIMIT 0,5';
							}
                                  include('connexion_database.php');
    $req = $conn->query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.$conn->mysqli_error());
    
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    while($data = $req->fetch_assoc())
    {	

								$d1 = new DateTime(date('d-m-Y' . $depart));
								$d2 = new DateTime(date('H:i:s', $data['temps']));
																
								//var_dump($d2);die;
								$diff = $d1->diff($d2); 
								// var_dump($diff); die;

                          ?>
                              <li class="list-group-item">
                                  <p class="pull-right text-danger h3"><?php print sprintf("%02d", $diff->h) . ":" . sprintf("%02d", $diff->i) . ":" . sprintf("%02d", $diff->s); ?></p>
                                  <a href="#"><p class="title"><?php print $data['nom'] . " " . $data['prenom']; ?></p></a>
                                  <p class="info">Dossard <?php print $data['dossard']; ?></p>
                              </li>
                        <?php
                            }
                        ?>
                    </ul>
        </div>
    </div>
        </div>

            <footer>
                <hr>
        <p class="pull-right">Les 15 kilomètres de Montigny</p>
                <p>© 2014 <a href="http://slebourg.fr" target="_blank">S.Lebourg</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
