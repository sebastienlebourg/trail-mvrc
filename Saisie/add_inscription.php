<?php 
if(isset($_GET['edit'])){

    $sql = 'SELECT *
              FROM inscription i
			  WHERE id = '.$_GET['edit'];

    include('connexion_database.php');
    $req = $conn->prepare($sql);
    $req->execute();
    
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    while($data = $req->fetch(PDO::FETCH_ASSOC))
    {			
      $row = $data;
    }	
  }			
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Trail de Noël 2019</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header" style="text-align:center; width:100%;">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span style="text-align:center; width:100%; text-transform:uppercase;" class="navbar-brand">Trail de Noël 2019</span></a>
		</div>
      </div>
    </div>
    

	<?php include('menu.html'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">Nouvelle inscription</h1>
			<ul class="breadcrumb">
				<li><a href="index.p">Accueil</a> </li>
				<li class="active">Inscriptions</li>
				<li class="active">Nouvelle inscription</li>
			</ul>
        </div>
                <div class="main-content">
            

<div class="row">
  <div class="col-md-4">
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
      <form id="tab" method="post" action="valider_inscription.php">
		<?php if(isset($_GET['edit'])){
		?>	
		<input type="hidden" name="id_edit" value="<?php echo $_GET['edit']; ?>" class="form-control">
		<?php	
		} ?>
        <div class="form-group">
			<label>Nom</label>
			<input type="text" name="nom" value="<?php if(isset($_GET['edit'])){echo $row["nom"];} ?>" class="form-control">
        </div>
		
        <div class="form-group">
			<label>Prénom</label>
			<input type="text" name="prenom" value="<?php if(isset($_GET['edit'])){echo $row["prenom"];} ?>" class="form-control">
        </div>
        <div class="form-group">
        <label>Club</label>
        <input type="text" name="club_nom" value="<?php if(isset($_GET['edit'])){echo $row["club_nom"];} ?>" class="form-control">
        </div>
        <div class="form-group">
			<label>Email</label>
			<input type="email" name="email" value="<?php if(isset($_GET['edit'])){echo $row["email"];} ?>" class="form-control">
        </div>
        <div class="form-group">
			<label>Téléphone</label>
			<input type="text" name="tel" value="<?php if(isset($_GET['edit'])){echo $row["tel"];} ?>" class="form-control">
        </div>
		
        <div class="form-group">
			<label>Date de naissance</label>
			<input type="text" name="naissance" value="<?php if(isset($_GET['edit'])){echo $row["naissance"];} ?>" class="form-control" placeholder="jj/mm/aaaa">
        </div>
		
        <div class="form-group">
            <label>Sexe</label>
            <select name="sexe" id="DropDownTimezone" class="form-control">
              <option value="aucun">Choisissez...</option>
              <option value="Homme" <?php if(isset($_GET['edit']) && $row["sexe"] == "Homme"){echo "selected";} ?>>Homme</option>
              <option value="Femme" <?php if(isset($_GET['edit']) && $row["sexe"] == "Femme"){echo "selected";} ?>>Femme</option>
            </select>
        </div>

        <div class="form-group">
            <label>Course</label>
            <select name="course" id="DropDownTimezone" class="form-control">
              <option value="21" <?php if(isset($_GET['edit']) && $row["course"] == "21"){echo "selected";}elseif(isset($_GET['course']) && $_GET["course"] == "21"){echo "selected";}  ?>>21 kilomètres</option>
              <option value="11" <?php if(isset($_GET['edit']) && $row["course"] == "11"){echo "selected";}elseif(isset($_GET['course']) && $_GET["course"] == "11"){echo "selected";} ?>>11 kilomètres</option>
            </select>
         </div>
		 
		<div class="form-group">
            <label>Certificat</label>
            <select name="certificat" id="DropDownTimezone" class="form-control">
              <option value="aucun">Choisissez...</option>
              <option value="a verifier" <?php if(isset($_GET['edit']) && !empty($row["certificat"]) && $row["certificat"] == "a verifier"){echo "selected";} ?>>A vérifier</option>
              <option value="oui" <?php if(isset($_GET['edit']) && !empty($row["certificat"]) && $row["certificat"] == "oui"){echo "selected";} ?>>Oui</option>
              <option value="non" <?php if(isset($_GET['edit']) && (empty($row["certificat"]) || $row["certificat"] == "non")){echo "selected";} ?>>Non</option>
            </select>
        </div>
		
    <div class="form-group">
            <label>Paiement</label>
            <select name="paiement" id="DropDownTimezone" class="form-control">
              <option value="aucun" >Choisissez...</option>
              <option value="valider" <?php if(isset($_GET['edit']) && $row["paiement"] == "valider"){echo "selected";} ?>>Valider</option>
              <option value="non" <?php if(isset($_GET['edit']) && $row["paiement"] != "valider"){echo "selected";} ?>>Non payé</option>
            </select>
        </div>
    <div class="form-group">
            <label>Type de paiement</label>
            <select name="type_paiement" id="DropDownTimezone" class="form-control">
              <option value="aucun">Choisissez...</option>
              <option value="papier" <?php if(isset($_GET['edit']) && $row["type_paiement"] == "papier"){echo "selected";} ?>>Papier</option>
              <option value="web" <?php if(isset($_GET['edit']) && $row["type_paiement"] == "web"){echo "selected";} ?>>Web</option>
            </select>
        </div>
      </div>
		
    </div>

    <div class="btn-toolbar list-toolbar">
      <input type="submit" value="Enregistrer" class="btn btn-danger">
      <input type="reset" value="Annuler" class="btn btn-danger">
    </div>

      </form>
  </div>
</div>

<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
      </div>
    </div>
  </div>
</div>


            <footer>
                <hr>
				<p class="pull-right">Trail de Noël 2019</p>
                <p>© 2014 <a href="#" target="_blank">S.Lebourg</a></p>
            </footer>
        </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
