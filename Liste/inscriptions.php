<?php
    include("refresh.php");
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Trail de Noël 2019</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header" style="text-align:center; width:100%;">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.php"><span style="text-align:center; width:100%; text-transform:uppercase;" class="navbar-brand">Trail de Noël 2019</span></a>
		</div>
      </div>
    </div>
    

	<?php include('menu.html'); ?>

    <div class="content" style="margin-left: 0 !important; text-align:center !important;">
        <div class="header">
            <h1 class="page-title">Inscriptions des <?php print $_GET['course']; ?> km</h1>
			<ul class="breadcrumb">
				<li></li>
				<li class="active">Inscriptions</li>
				<li class="active">Inscriptions des <?php print $_GET['course']; ?> km</li>
			</ul>
        </div>
                <div class="main-content">
            
<div class="btn-toolbar list-toolbar">
    <form action="inscriptions.php?course=<?php echo $_GET['course'] . '&'; ?>action=search" method="post" style="float:right;">
		<input type="text" name="search">
		<input type="submit" class="btn btn-default" value="Rechercher">
	</form>
  <div class="btn-group">
  </div>
</div>
<table class="table" style="text-align:center !important;">
  <thead style="text-align:center !important;">
    <tr style="text-align:center !important;">
      <th style="text-align:center !important;">Nom</th>
      <th style="text-align:center !important;">Prénom</th>
      <th style="text-align:center !important;">Club</th>
      <th style="text-align:center !important;">État du dossier</th>
    </tr>
  </thead>
  <tbody>
    <?php
		// $sql_dossard = 'SELECT debut_dossard from attribution_dossard WHERE course LIKE "'.$_GET["course"].'"';
        
    // include('connexion_database.php');
    // $req_dossard = mysql_query($sql_dossard) or die('Erreur SQL !<br>'.$sql_dossard.'<br>'.mysql_error());
     // $dossard = '500';
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    // while($data_dossard = mysql_fetch_assoc($req_dossard))
    // {
      // $dossard = $data_dossard['debut_dossard'];
    // }

    if(isset($_GET['action'])){
      $sql = 'SELECT *
              FROM inscription i
              WHERE course = '.$_GET["course"] . ' 
              AND (nom LIKE "%'.$_POST["search"].'%" OR prenom LIKE "%'.$_POST["search"].'%")
			  AND (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }elseif(isset($_GET['course'])){
      $sql = 'SELECT *
        FROM inscription i
        WHERE course = "'.$_GET["course"] . '"
		AND (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }else{
      $sql = 'SELECT *
              FROM inscription i
			  WHERE (paiement = "valider"  OR paiement = "non") ORDER BY nom';
    }

    include('connexion_database.php');
    $req = $conn->prepare($sql);
    $req->execute();
    
    // on fait une boucle qui va faire un tour pour chaque enregistrement
    while($data = $req->fetch(PDO::FETCH_ASSOC))
    {							
		?>
			<tr <?php if($data['certificat'] == "non"){ ?>style="background: red !important"<?php } ?>>
			  <td><?php print $data['nom']; ?></td>
			  <td><?php print $data['prenom']; ?></td>
			  <td><?php print $data['club_nom']; ?></td>
    <td><?php if($data['certificat'] == "non"){ ?>Dossier incomplet<?php }else if($data['certificat'] == "a verifier"){ ?>Dossier en cours de validation<?php }else{ ?>Dossier validé<?php } ?></td>
			</tr>
		<?php		
    }
	?>
  </tbody>
</table>

            <footer>
                <hr>
				<p class="pull-right">Trail de Noël 2019</p>
                <p>© 2015 <a href="#" target="_blank">S.Lebourg</a></p>
            </footer>
        </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
