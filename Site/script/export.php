<?php


    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);


	// On définit les 4 variables nécessaires à la connexion MySQL :
	$hostname = "traildenrjtrail.mysql.db";
	$port = 3306;
	$user     = "traildenrjtrail";
	$password = "Nico3f6a";
	$nom_base_donnees = "traildenrjtrail";

	try
	{
		$connexion = new PDO('mysql:host='.$hostname.';port='.$port.';dbname='.$nom_base_donnees, $user, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $connexion->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        
        $sql = "SELECT b.id, b.customerId, customFields, info, e.eventPeriodId as course FROM wp_amelia_customer_bookings b, wp_amelia_customer_bookings_to_events_periods e WHERE b.id = e.customerBookingId";
        $req = $connexion->prepare($sql);
        $req->execute();

        $inscriptions = array();

        while($data = $req->fetch(PDO::FETCH_ASSOC))
        {
            $arrData[] = $data;
            
        }

        foreach($arrData as $data){
            
            $customFields = json_decode($data["customFields"]);
            $infos = json_decode($data["info"]);
            $one = "1";
            $deux = "2";
            $trois = "3";

            $inscription = array(
                "id" => $data["id"],
                "course" => ($data["course"] == "1" ? "11,5 km" : "21 km"),
                "prenom" => $infos->firstName,
                "nom" => $infos->lastName,
                "tel" => $infos->phone,
                "fichier" => !empty($customFields->{"1"}->value["0"]->fileName) ? "http://traildenoel.fr/wp-content/uploads/amelia/files/".$data["id"]."_".$customFields->{"1"}->value["0"]->fileName : "",
                "naissance" => $customFields->{"2"}->value,
                "sexe" => $customFields->{"3"}->value
            );

            $inscriptions[] = $inscription;
            

        }

        $outputBuffer = fopen("php://output", 'w');
        foreach($inscriptions as $val) {
            fputcsv($outputBuffer, $val, ";");
        }
        fclose($outputBuffer);

	}
	catch(Exception $e)
	{
		echo 'Erreur : '.$e->getMessage().'<br />';
		echo 'N° : '.$e->getCode();
		die();
    }

    die('OK');

?>